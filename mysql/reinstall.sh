#!/bin/bash

cd /usr/local/mysql
#(ls backup || mkdir backup) && cp -rf * backup/
rm -rf data/*
./mysql_install_db --defaults-file=/etc/my.cnf --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data --user=mysql
cp -rf backup/* data/
chown -R mysql:mysql /usr/local/mysql
