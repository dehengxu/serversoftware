#!/usr/bin/bash

#Add mysql user
useradd mysql

mypath=/usr/local/mysql
ls /etc/my.cnf && mv /etc/my.cnf  /etc/my.cnf.old
ls /etc/my.cnf || cp -f $mypath/support-files/my-default.cnf  /etc/my.cnf

echo "innodb_buffer_pool_size = 4M">>/etc/my.cnf
echo "basedir = /usr/local/mysql">>/etc/my.cnf
echo "datadir = /usr/local/mysql/data">>/etc/my.cnf
echo "port = 3306">>/etc/my.cnf
echo "server_id = 1">>/etc/my.cnf
echo "socket = /tmp/mysql.sock">>/etc/my.cnf

echo "join_buffer_size = 4M">>/etc/my.cnf
echo "sort_buffer_size = 2M">>/etc/my.cnf
echo "read_rnd_buffer_size = 2M">>/etc/my.cnf

#This line always be default in my-default.cnf
#echo "sql_mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES">>/etc/my.cnf

chown -R mysql:mysql /usr/local/mysql

cd /usr/local/mysql/
cp scripts/mysql_install_db ./
chmod +x mysql_install_db
./mysql_install_db --defaults-file=/etc/my.cnf --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data --user=mysql
./bin/mysqld_safe & && echo "Started mysqld_safe"
echo "Please use mysqladmin -u root password 'you password' init a root user in mysql system."

chmod +x ./support-files/mysql.server
cp ./support-files/mysql.server  /etc/init.d/mysql
chown root:root /etc/init.d/mysql
chkconfig -a mysql

