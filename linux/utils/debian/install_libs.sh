!#/bin/bash
apt-get -y install gcc g++ make cmake chkconfig
apt-get -y install build-essential
apt-get -y install zip rar p7zip*
apt-get -y install p7zip-full
apt-get -y install vim sudo git
apt-get -y install libpcre*
apt-get -y install libxml2-dev libxslt-dev libcurl4-openssl-dev libjpeg8-dev libpng12-dev libvpx-dev libfreetype6-dev libmcrypt-dev
apt-get -y install libncurses5-dev libpthread*  bison flex
apt-get -y install libtools*
apt-get -y install automake gettext
apt-get clean
exit
