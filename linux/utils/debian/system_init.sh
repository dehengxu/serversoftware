!#/bin/bash

#root user
adduser nicholasxu
mkdir /home/nicholasxu/backup
mkdir /home/nicholasxu/backup/db
mkdir /home/nicholasxu/backup/web
mkdir /home/nicholasxu/tools
chown -R nicholasxu:nicholasxu /home/nicholasxu

mkdir /home/wwwroot/download
ln -s /home/nicholasxu/backup  /home/wwwroot/download/backup
ln -s /home/nicholasxu/tools  /home/wwwroot/download/tools


echo "nicholasxu ALL (ALL:AL) ALL" >> /etc/sudouser
cp -r /root/.ssh /home/nicholasxu
chown -R nicholasxu:nicholasxu /home/nicholasxu/.ssh

#Upgrade
apt-get update
apt-get -y upgrade
apt-get -y dist-upgrade
apt-get install chkconfig

apt-get install apt-spy
apt-spy -d testing -a North-America -t 1
apt-get update
apt-get dist-upgrade

useradd www
mkdir /home/wwwroot
chown -R /home/wwwroot
mkdir /home/wwwlogs
chown -R /home/wwwlogs

apt-get install -y nginx
apt-get install -y mysql-server
apt-get install -y libmysqlclient18
apt-get install -y nodejs php5 python python3

chkconfig apache2 off

