#!/bin/bash
#Assume mysql installed under root /usr/local/mysql
#Assume php installed under root /usr/local/php
#Assume php installed under root /usr/local/nginx
#

useradd www
mkdir /home/wwwroot
mkdir /home/wwwlogs
chown -R www /home/wwwroot
chown -R www /home/wwwlogs

useradd mysql
chown -R mysql:mysql /usr/local/mysql
cd /usr/local/mysql
cp -rf ./support-files/my-default.cnf   /etc/my.cnf
cp -rf ./support-files/mysql.server /etc/init.d/mysql
chown -R root:staff /etc/init.d/mysql
chmod +x /etc/init.d/mysql

cd /usr/local/php
cp -rf ./etc/php-fpm.conf.default  ./etc/php-fpm.conf

