All operation under Debian Linux.

Uncompress src package like mysql-version.tar.gz

Into src folder & input below:

### Configurate project , make and installing.

    sudo cmake -DCMAKE_INSTALL_PREFIX=/usr/local/mysql -DMYSQL_UNIX_ADDR=/tmp/mysql.sock -DDEFAULT_CHARSET=utf8 -DDEFAULT_COLLATION=utf8_general_ci -DWITH_EXTRA_CHARSETS:STRING=utf8,gbk -DWITH_MYISAM_STORAGE_ENGINE=1 -DWITH_INNOBASE_STORAGE_ENGINE=1 -DWITH_READLINE=1 -DENABLED_LOCAL_INFILE=1 -DMYSQL_DATADIR=/var/mysql/data
    sudo make
    sudo make install

### create mysql manage user and group

    sudo groupadd mysql
    sudo useradd -r -g mysql mysql

### I use /var/mysql/data for saving data file, /usr/local/mysql as mysql dir, /etc/my.cnf as mysql config file, so I ...

    sudo chown -R mysql:mysql /var/mysql
    sudo chown -R mysql:mysql /usr/local/mysql
    cp support-files/my-medium.cnf /etc/my.cnf

### After configuration mysql settings, now let's make mysql starting, step by step.

#### Enter folder

    cd /usr/local/mysql
    
#### Configurate mysql runtime environment.

    sudo scripts/mysql_install_db --user=mysql --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data &
    
#### Starting daemon process..

    sudo bin/mysqld_safe --user=mysql &
    
#### Now we create a new mysql user

    sudo bin/mysqladmin -u root password 'you_password'

#### Testing 

    bin/mysql -u root -p

### entering you password in last step. Congratuations! You enter world of MySQL.

#### At last, set auto start service...

    cp support-files/mysql.server /etc/init.d/mysql.server
    #you can manually control mysql as 'start' 'restart' 'stop'
    sudo /etc/init.d/mysql.server start
    chmod 755 /etc/init.d/mysql.server
    chkconfig --add mysql.server  #Maybe you need install 'chkconfig' program.
    or
    chkconfig --add --level 345 mysql

---------
Solve some problem 

    ./scripts/mysql_install_db --no-defaults
    ./bin/mysqld_safe --no-defaults &
    ./bin/mysqladmin -u root password 'some characters'




