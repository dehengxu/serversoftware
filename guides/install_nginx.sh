#!/bin/sh

$file=nginx-$1.tar.gz
#Download nginx package code can be put here.

#Decompress nginx package.

#Compile installing
cd $file
sudo ./configure --prefix=/usr/local/nginx --with-debug --with-http_dav_module --with-http_stub_status_module
sudo make
sudo make install
cd ..

echo $file "Installing finished!"
