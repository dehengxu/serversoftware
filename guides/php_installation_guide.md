### Compiling php 

      sudo apt-get install libxpm-dev  #required
      sudo apt-get install libfreetype6-dev     #required
      
      ./configure --prefix=/usr/local/php --with-config-file-path=/usr/local/php/etc --with-mysql=/usr/local/mysql --with-mysqli=/usr/local/mysql/bin/mysql_config --with-iconv-dir --with-freetype-dir --with-jpeg-dir --with-png-dir --with-zlib --with-libxml-dir=/usr --enable-xml --enable-discard-path --enable-magic-quotes --enable-safe-mode --enable-bcmath --enable-shmop --enable-sysvsem --enable-inline-optimization --with-curl --with-curlwrappers --enable-mbregex --enable-fastcgi --enable-fpm --enable-force-cgi-redirect --enable-mbstring --with-mcrypt --enable-ftp --with-gd --enable-gd-native-ttf --with-openssl --with-mhash --enable-pcntl --enable-sockets --with-xmlrpc --enable-zip --enable-soap --without-pear --with-gettext --with-mime-magic --with-apxs2=/usr/local/apache2/bin/apxs
      sudo make
      sudo make install

### Configure & starting fpm(Fastcgi Process Managment)

#### Editing  php-fpm.conf
      
      cd /usr/local/php
      cp etc/php-fpm.conf.default  etc/php-fpm.conf
      sudo vi etc/php-fpm.conf
      
	  		# Input belows:
            listen = /tmp/php-cgi.sock #Specifying use unix sock file path of php, it autogenerate php-cgi.sock after php-fpm started. By editing here ,you can use  127.0.0.1:9000 process php script。
            user = www
            group = www
            pm = dynamic
            pm.max_children = 20
            pm.start_servers = 2
            pm.min_spare_servers = 1
            pm.max_spare_servers = 3
      
			# Create user and group
			sudo groupadd www
			sudo useradd -g www www

      
#### Running php-fpm
      
		  sudo /usr/local/php/sbin/php-fpm

      
