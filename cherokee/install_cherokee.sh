#!/bin/bash
./autogen.sh --prefix=/usr/local/cherokee --with-wwwuser=www --with-wwwgroup=www --with-php=/usr/local/php/sbin/php-fpm --with-mysql
make && make install
exit
