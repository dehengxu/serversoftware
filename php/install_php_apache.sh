#--with-apxs2filter=FILE    EXPERIMENTAL: Build shared Apache 2.0 Filter module. FILE is the optional
#--with-apxs2=FILE     Build shared Apache 2.0 Handler module. FILE is the optional
#--with-apxs=FILE      Build shared Apache 1.x module. FILE is the optional
#                          pathname to the Apache apxs tool apxs
#  --with-apache=DIR     Build Apache 1.x module. DIR is the top-level Apache
#                          build directory /usr/local/apache
#  --enable-mod-charset      APACHE: Enable transfer tables for mod_charset (Rus Apache)

apt-get -y install libxml2-dev libxslt-dev libcurl4-openssl-dev libjpeg8-dev libpng12-dev libvpx-dev libfreetype6-dev libmcrypt-dev
./configure --prefix=/usr/local/php --with-config-file-path=/usr/local/php/etc --with-mysql=/usr/local/mysql --with-mysqli=/usr/local/mysql/bin/mysql_config --with-iconv-dir=/usr/local --with-freetype-dir=/opt/local --with-jpeg-dir --with-png-dir --with-zlib --with-libxml-dir=/usr --enable-xml --enable-discard-path --enable-magic-quotes --enable-safe-mode --enable-bcmath --enable-shmop --enable-sysvsem --enable-inline-optimization --with-curl --with-curlwrappers --enable-mbregex --enable-fastcgi --enable-fpm --enable-force-cgi-redirect --enable-mbstring --with-mcrypt --enable-ftp --with-gd --enable-gd-native-ttf --with-openssl --with-mhash --enable-pcntl --enable-sockets --with-xmlrpc --enable-zip --enable-soap --without-pear --with-gettext --with-mime-magic --with-apxs2=/usr/local/apache2/bin/apxs
make clean && make && make install
