#!/bin/bash

#install required libs
chmod +x ../linux/utils/debian/install_libs.sh
./linux/utils/debian/install_libs.sh

rm -rf ../apache2/apr-1.4.8
rm -rf ../apache2/apr-util-1.5.2
rm -rf ../apache2/apr-iconv-1.2.1
rm -rf ../apache2/httpd-2.4.6

echo "apr installing"
ls ../apache2/apr-1.4.8/ || tar -jxvf ../apache2/apr-1.4.8.tar.bz2 -C ../apache2
cd ../apache2/apr-1.4.8
./configure --prefix=/usr/local/apr
make && make install
cd ../../ServerSoftware

echo "apr-iconv installing"
ls ../apache2/apr-iconv-1.2.1/ || tar -jxvf ../apache2/apr-iconv-1.2.1.tar.bz2 -C ../apache2
cd ../apache2/apr-iconv-1.2.1
./configure --prefix=/usr/local/apr-iconv --with-apr=/usr/local/apr
make && make install
cd ../../ServerSoftware

echo "apr-util installing"
ls ../apache2/apr-util-1.5.2/ || tar -jxvf ../apache2/apr-util-1.5.2.tar.bz2 -C ../apache2
cd ../apache2/apr-util-1.5.2
./configure --prefix=/usr/local/apr-util --with-crypto --with-apr=/usr/local/apr --with-mysql=/usr/local/mysql
make && make install
cd ../../ServerSoftware


#echo "apr installing"
#ls ../apache2/libapreq2-2.13/ || tar -zxvf ../apache2/libapreq2-2.13.tar.gz -C ../apache2
#cd ../apache2/libapreq2-2.13
#./configure --prefix=/usr/local/apr-util
#make && make install
#cd ../../ServerSoftware

echo "httpd installing"
ls ../apache2/httpd-2.4.6/ || tar -jxvf ../apache2/httpd-2.4.6.tar.bz2 -C ../apache2
cd ../apache2/httpd-2.4.6
./configure --prefix=/usr/local/apache2 --with-apr=/usr/local/apr --with-apr-util=/usr/local/apr-util
make && make install
cd ../../ServerSoftware


echo "php installing"
ls ../php-5.5.1/ || tar -xvf ../php-5.5.1.tar.xz -C ../
cp -rf php/install_php_apache.sh ../php-5.5.1
chmod +x ../php-5.5.1/install_php_apache.sh
cd ../php-5.5.1
sudo ./install_php_apache.sh
cd ../ServerSoftware




