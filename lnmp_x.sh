#!/bin/bash

#Assume you have download files.Directory structer like belows:
#current
#nginx-1.5.3.tar.gz
#nginx-1.5.3/
#...
#...
#./ServerSoftware
# ...
# ...

#Intalling some lib tools.
chmod +x linux/utils/debian/install_libs.sh
sudo ./linux/utils/debian/install_libs.sh

#compile, install
echo "Clean old directories."
sudo rm -rf ../nginx-1.5.3/
sudo rm -rf ../php-5.5.1/
sudo rm -rf ../mysql-5.6.13/

echo "Nginx installing"
ls ../nginx-1.5.3/ || tar -zxvf ../nginx-1.5.3.tar.gz -C ../
cp -rf nginx/install_nginx.sh  ../nginx-1.5.3/
chmod +x ../nginx-1.5.3/install_nginx.sh
cd ../nginx-1.5.3/
sudo ./install_nginx.sh
cd ../ServerSoftware

echo "mysql installing"
ls ../mysql-5.6.13/ || tar -zxvf ../mysql-5.6.13.tar.gz -C ../
cp -rf mysql/install_mysql.sh  ../mysql-5.6.13
chmod +x ../mysql-5.6.13/install_mysql.sh
cd ../mysql-5.6.13
sudo ./install_mysql.sh
cd ../ServerSoftware

echo "php installing"
ls ../php-5.5.1/ || tar -xvf ../php-5.5.1.tar.xz -C ../
cp -rf php/install_php.sh  ../php-5.5.1
chmod +x ../php-5.5.1/install_php.sh
cd ../php-5.5.1
sudo ./install_php.sh
cd ../ServerSoftware


#configure