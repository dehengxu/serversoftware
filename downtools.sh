#!/bin/bash
ls ./cherokee || mkdir cherokee
cd ./cherokee
#ls v1.2.103.zip || wget https://github.com/cherokee/webserver/archive/v1.2.103.zip
ls webserver || git clone https://github.com/cherokee/webserver.git
cd ../

ls mysql-5.6.13.tar.gz || wget http://cdn.mysql.com/Downloads/MySQL-5.6/mysql-5.6.13.tar.gz
ls nginx-1.5.3.tar.gz || wget http://nginx.org/download/nginx-1.5.3.tar.gz
ls php-5.5.1.tar.xz || wget "http://www.php.net/get/php-5.5.1.tar.xz/from/us1.php.net/mirror" -O php-5.5.1.tar.xz

exit 0

#If apache2 doesn't exist then create it.

ls ./apache2 || mkdir apache2
cd ./apache2
ls httpd-2.4.6.tar.bz2 || wget http://apache.etoak.com//httpd/httpd-2.4.6.tar.bz2
ls apr-1.4.8.tar.bz2 || wget http://mirrors.cnnic.cn/apache//apr/apr-1.4.8.tar.bz2
ls apr-util-1.5.2.tar.bz2 || wget http://mirrors.cnnic.cn/apache//apr/apr-util-1.5.2.tar.bz2
ls apr-iconv-1.2.1.tar.bz2 || wget http://mirrors.cnnic.cn/apache//apr/apr-iconv-1.2.1.tar.bz2
ls libapreq2-2.13.tar.gz || wget http://apache.fayea.com/apache-mirror//httpd/libapreq/libapreq2-2.13.tar.gz
cd ..

exit 0