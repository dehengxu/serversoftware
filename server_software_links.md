#***Software, libraries for linux server***#
###*Svn src*
[SVN ](http://archive.apache.org/dist/subversion/)

###*Webmin*
[Webmin ](http://prdownloads.sourceforge.net/sourceforge/webadmin/webmin_1.650_all.deb)

###*Nginx*
[Home](http://www.nginx.org/)

[stable Nginx1.4.2 src](http://nginx.org/download/nginx-1.4.2.tar.gz)

[mainline Nginx1.5.5 src](http://nginx.org/download/nginx-1.5.5.tar.gz)

###*PHP*
[php 5.5.4](http://cn2.php.net/get/php-5.5.4.tar.xz/from/us1.php.net/mirror)

[php 5.5.1](http://www.php.net/get/php-5.5.1.tar.xz/from/us1.php.net/mirror)

[php 5.4.17](http://www.php.net/get/php-5.4.17.tar.bz2/from/us1.php.net/mirror)

[php 5.3.27](http://www.php.net/get/php-5.3.27.tar.bz2/from/us1.php.net/mirror)
PS:php-5.4 和 dokuwiki 不兼容。

###*Mysql*
[Mysql 5.6.10 Download](http://cdn.mysql.com/Downloads/MySQL-5.6/mysql-5.6.10.tar.gz)

[Mysql 5.6.11 Download](http://cdn.mysql.com/Downloads/MySQL-5.6/mysql-5.6.11.tar.gz)

[Mysql 5.6.13 Download](http://cdn.mysql.com/Downloads/MySQL-5.6/mysql-5.6.13.tar.gz)

###*phpMyAdmin*
[Download page](http://www.phpmyadmin.net/home_page/downloads.php)

###*Cherokee*
[Home](http://cherokee-project.com/)
[Cherokee v1.2.103](https://github.com/cherokee/webserver/archive/v1.2.103.zip)

###*Apache2.4*
[Home](http://httpd.apache.org)

[Apache2.4.4 src](http://apache.etoak.com//httpd/httpd-2.4.4.tar.gz)

[Apache2.4.6](http://apache.etoak.com//httpd/httpd-2.4.6.tar.bz2)

###*APR - Apache Portable Runtime*
[Home](http://apr.apache.org/)

[APR src](http://mirrors.cnnic.cn/apache//apr/apr-1.4.8.tar.bz2)

[APR-util src](http://mirrors.cnnic.cn/apache//apr/apr-util-1.5.2.tar.bz2)

[APR-iconv src](http://mirrors.cnnic.cn/apache//apr/apr-iconv-1.2.1.tar.bz2)

[libapreq2 ](http://apache.fayea.com/apache-mirror//httpd/libapreq/libapreq2-2.13.tar.gz)


###*Apache2.2*
[Apache2.2.24 src](http://apache.etoak.com//httpd/httpd-2.2.24.tar.gz)

[J2EE 7](http://download.oracle.com/otn-pub/java/java_ee_sdk/7/java_ee_sdk-7-unix-ml.sh?AuthParam=1377244348_db904fb38f8dc7175f9cad02dd90b0b4)

###*uWsgi*
[Home](http://projects.unbit.it/uwsgi/)

[uWsgi](https://github.com/unbit/uwsgi.git)

[uWsgi](http://projects.unbit.it/downloads/uwsgi-1.9.14.tar.gz)

###*Python*
*Linux:*
[Python 3.3.0](http://www.python.org/ftp/python/3.3.0/Python-3.3.0.tgz)
[Python 2.7.3](http://www.python.org/ftp/python/2.7.3/Python-2.7.3.tgz)

*Macos (intel x86 i386):*
[Python 3.3.0](http://www.python.org/ftp/python/3.3.0/python-3.3.0-macosx10.6.dmg)

[Python 2.7.3](http://www.python.org/ftp/python/2.7.3/python-2.7.3-macosx10.6.dmg)

*Windows:*
[Python 3.3.0](http://www.python.org/ftp/python/3.3.0/python-3.3.0.msi)

[Python 2.7.3](http://www.python.org/ftp/python/2.7.3/python-2.7.3.msi)


###*openssl*
[Home](http://www.openssl.org/source/)

[openSSL-1.0.1 src](ftp://ftp.openssl.org/source/openssl-1.0.1e.tar.gz)

###*Curl*
[Curl src](http://curl.haxx.se/download/curl-7.30.0.tar.gz)

###*freetype*
[freetype src](http://download.savannah.gnu.org/releases/freetype/freetype-2.4.12.tar.gz)

###*libpng*
[Home](http://libmng.com/pub/png/libpng.html)

###*libjpeg*
[Home](http://www.ijg.org/)

###*zlib*
[Home](http://www.zlib.net/)

###*libxml2*
[Home](http://www.xmlsoft.org/)

[libxml2 src](ftp://xmlsoft.org/libxml2/libxml2-git-snapshot.tar.gz)

###*libxslt*
[libxslt 1.1.28](https://git.gnome.org/browse/libxslt/snapshot/libxslt-1.1.28.tar.gz)

###*libmcrypt*
[Home](http://mcrypt.hellug.gr/lib/index.html)

[Download](ftp://mcrypt.hellug.gr/pub/crypto/mcrypt/libmcrypt/libmcrypt-2.5.7.tar.gz)

###*libtool*
[Home](http://www.gnu.org/software/libtool)

[libtool-2.4.2 src](http://ftpmirror.gnu.org/libtool/libtool-2.4.2.tar.gz)

###*libxpm*
[libxpm src](http://cgit.freedesktop.org/xorg/lib/libXpm/snapshot/libXpm-3.5.10.tar.gz)

###*GNU m4*
[Home](http://www.seindal.dk/rene/gnu/modules.htm)

[m4 src](ftp://ftp.seindal.dk/pub/rene/gnu/m4-1.4.tar.gz)

###*libpcre*
[Home](http://www.pcre.org/)
[libpcre-8.33](ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.33.tar.gz)




